# Index.html

Jeg har foretaget nogle valg under opbygningen af siden. de er kort beskrevet i punktform her:
* Logo er "hjem" knappen
* Menuen er flyttet ud i sin egen section, jf prototypen (kunne også være inden i <head>

* <main> er inddelt i en <section> og en <article>. 
  Årsagen her til er at billdet har sin beskrivende tekst i venstre <article>
* Overskrifterne er <h2>

* <aside> er ligesom <main> indelt i to
  Her mener jeg har videoen kan stå alene og derfor i en <article>
  Henvisningen til Google Play kan måske også stå alene, men idet indholdet
  af den skrevne tekst ikke kendes, er den skal til <section>
  
* <footer> er lavet således at telefon og mail er <a> links. 

Thomas